from django.forms import fields
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.urls.base import reverse
from django.views.generic import TemplateView, ListView, CreateView, UpdateView, DeleteView
from django.contrib.auth.views import LoginView, logout_then_login
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from . import forms
from . import models

# Create your views here.


########################################################################
##                            Authentication                          ##
########################################################################

@login_required
def signout(request):
    return logout_then_login(request)


class SigninView(LoginView):
    template_name = "pages/auth/signin.html"
    authentication_form = forms.LoginForm


class AccountView(ListView):
    template_name = "pages/auth/account.html"
    model = models.User


########################################################################
##                                 Home                               ##
########################################################################

@method_decorator(login_required, name='dispatch')
class HomeView(TemplateView):
    template_name = 'pages/index.html'


@method_decorator(login_required, name='dispatch')
class LeaderboardView(TemplateView):
    template_name = 'pages/leaderboard.html'


########################################################################
##                                  User                              ##
########################################################################

@method_decorator(login_required, name='dispatch')
class UserListView(ListView):
    template_name = "pages/users/users.html"
    model = models.User


@method_decorator(login_required, name='dispatch')
class UserDetailsView(UpdateView):
    template_name = "pages/users/user_details.html"
    model = models.User
    fields = [
        'last_name', 
        'first_name',
        'gender', 
        'register', 
        'email', 
        'phone', 
        'username', 
        'is_superuser',
    ]


@method_decorator(login_required, name='dispatch')
class UserCreateView(CreateView):
    template_name = "pages/users/user_create.html"
    success_url = reverse_lazy('eatsj:user_create')
    model = models.User
    form_class = forms.UserCreateForm


@method_decorator(login_required, name='dispatch')
class UserDeleteView(DeleteView):
    model = models.User
    success_url = reverse_lazy('eatsj:user_list')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    # def get_success_url(self) -> str:
    #     return reverse('eatsj:user_list')


@login_required
def user_delete_view(request, pk):
    get_object_or_404(models.User, pk=pk).delete()
    return redirect(reverse('eatsj:user_list'))


# @method_decorator(login_required, name='dispatch')
# class UsersView(View):

#     def get(self, request, *args, **kwargs):
#         view = _UserListAbst.as_view()
#         return view(request, *args, **kwargs)
    
#     def post(self, request, *args, **kwargs):
#         view = _UserCreateAbst.as_view()
#         return view(request, args, **kwargs)


########################################################################
##                                 Menu                               ##
########################################################################

@method_decorator(login_required, name='dispatch')
class ProductsMenuListView(ListView):
    template_name = "pages/products/menus.html"
    model = models.Food


@method_decorator(login_required, name='dispatch')
class ProductsMenuCreateView(TemplateView):
    template_name = "pages/products/menus_create.html"


########################################################################
##                                 Food                               ##
########################################################################

@method_decorator(login_required, name='dispatch')
class ProductsFoodListView(ListView):
    template_name = "pages/products/foods.html"
    model = models.Food


@method_decorator(login_required, name='dispatch')
class ProductsFoodCreateView(CreateView):
    template_name = "pages/products/food_create.html"
    success_url = reverse_lazy('eatsj:products_food_list')
    model = models.Food
    fields = [
        'name',
        'image',
        'price_init',
        'description',
    ]


@method_decorator(login_required, name='dispatch')
class ProductsFoodDetailsView(UpdateView):
    template_name = "pages/products/food_details.html"
    model = models.Food
    fields = [
        'name',
        'image',
        'price_init',
        'description',
    ]


@login_required
def product_food_delete_view(request, pk):
    get_object_or_404(models.Food, pk=pk).delete()
    return redirect(reverse('eatsj:products_food_list'))


########################################################################
##                                 Drink                              ##
########################################################################

@method_decorator(login_required, name='dispatch')
class ProductsDrinkListView(ListView):
    template_name = "pages/products/drinks.html"
    model = models.Drink


@method_decorator(login_required, name='dispatch')
class ProductsDrinkCreateView(CreateView):
    template_name = "pages/products/drink_create.html"
    success_url = reverse_lazy('eatsj:products_drink_list')
    model = models.Drink
    fields = [
        'name',
        'image',
        'price_init',
        'nbr_liter',
    ]


@method_decorator(login_required, name='dispatch')
class ProductsDrinkDetailsView(UpdateView):
    template_name = "pages/products/drink_details.html"
    model = models.Drink
    fields = [
        'name',
        'image',
        'price_init',
        'nbr_liter',
    ]


@login_required
def product_drink_delete_view(request, pk):
    get_object_or_404(models.Drink, pk=pk).delete()
    return redirect(reverse('eatsj:products_drink_list'))


@method_decorator(login_required, name='dispatch')
class OrdersListView(ListView):
    template_name = "pages/orders/orders.html"
    model = models.Order


@method_decorator(login_required, name='dispatch')
class HistoriesListView(ListView):
    template_name = "pages/histories.html"
    model = models.History

    # def get_queryset(self):
    #     return self.model.objects.filter()
