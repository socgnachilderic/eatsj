import 'package:flutter/material.dart';

const defaultTextColor = Colors.black;
const predefaultTextColor = Colors.white;

const primaryColor = Color(0xFF8A0717);
const secondaryColor = Color(0xFFD40C26);
const backgroundColor = Colors.white;
const gradientBackground = BoxDecoration(
    gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [secondaryColor, primaryColor]));

const defaultFont = 'Montserrat';
const titleFont = 'Comfortaa';

class MyTheme {
  static final ThemeData defaultTheme = _buildMyTheme();

  static _buildMyTheme() {
    final ThemeData base = ThemeData.light();

    return base.copyWith(
      primaryColor: primaryColor,
      accentColor: secondaryColor,
    );
  }
}
