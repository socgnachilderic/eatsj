import 'dart:convert';

class Token {
  final String access;
  final String refresh;

  Token({this.access, this.refresh});

  Token.fromJson(Map<String, dynamic> data)
      : this.access = data['access'] as String,
        this.refresh = data['refresh'];

  factory Token.parseToken(String data) {
    var parsed = json.decode(data);
    var token = Token.fromJson(parsed);
    return token;
  }
}
