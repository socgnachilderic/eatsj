import 'package:eatsj_mobile/models/Token.dart';
import 'package:eatsj_mobile/utils/configs.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:eatsj_mobile/components/roundedInput.dart';
import '../utils/themes.dart';

class LoginScreen extends StatelessWidget {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)),
      );

  Future<Token> attemptLogIn(String username, String password) async {
    var res = await http.post("$SERVER_IP/token/",
        body: {"username": username, "password": password});

    if (res.statusCode == 200) return Token.parseToken(res.body);
    return null;
  }

  Future<int> attemptSignUp(String username, String password) async {
    var res = await http.post('$SERVER_IP/signup',
        body: {"username": username, "password": password});
    return res.statusCode;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: 48, left: 24, right: 24),
        decoration: gradientBackground,
        child: Center(
          child: Column(
            children: <Widget>[
              Container(
                width: 125,
                height: 125,
                margin: EdgeInsets.only(top: 30, bottom: 10),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, .32),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Transform.rotate(
                  angle: 15,
                  child: Container(
                    margin: EdgeInsets.all(20),
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColorDark,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Transform.rotate(
                      angle: -15,
                      // child: SvgPicture.asset(),
                      child: Image.asset(
                        'assets/images/logo.png',
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Text(
                        "eat'sj",
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      Text(
                        "Mangez chez nous par plaisir.",
                        style: Theme.of(context).textTheme.headline6,
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 50,
              ),
              Container(
                child: Column(
                  children: <Widget>[
                    RoundedInput(
                      placeholder: "Nom d'utilisateur",
                      controller: _usernameController,
                      icon: Icons.person,
                    ),
                    RoundedInput(
                      placeholder: "Mot de passe",
                      controller: _passwordController,
                      icon: Icons.lock,
                      secured: true,
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    FlatButton(
                      onPressed: () async {
                        var username = _usernameController.text;
                        var password = _passwordController.text;
                        var token = await attemptLogIn(username, password);
                        if (token.access != null) {
                          storage.write("token", token.access);
                          storage.write("refreshToken", token.refresh);
                          Navigator.pushNamed(context, '/');
                        } else {
                          displayDialog(context, "Erreur d'authentification",
                              "Aucun compte n'a été trouvé correspondant à ce nom d'utilisateur et à ce mot de passe");
                        }
                      },
                      child: Container(
                        height: 50,
                        width: size.width * .8,
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColorDark,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: Center(
                          child: Text(
                            'Connexion',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Montserrat',
                            ),
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "N'avez vous pas un compte ?",
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        FlatButton(
                          onPressed: () => print("object"),
                          child: Text(
                            "S'inscrire",
                            style: TextStyle(
                              color: Theme.of(context).primaryColorDark,
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    FlatButton(
                      onPressed: () => print("object"),
                      child: Text(
                        "Mot de passe oublié ?",
                        style: TextStyle(
                          color: Theme.of(context).primaryColorDark,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
